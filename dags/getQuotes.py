from airflow.providers.mongo.hooks.mongo import MongoHook
from airflow import DAG
from airflow.contrib.hooks.bigquery_hook import BigQueryHook
from airflow.operators.python_operator import PythonOperator
from datetime import datetime
from datetime import timedelta
import pandas as pd
from airflow.models import Variable
from airflow.exceptions import AirflowSkipException

relation1 = {
   "bigquery":{
      "pre":{
         "conn_id": "gcs_data"
      },
      "prod":{
         "conn_id": "gcs_data"
      }
   },
   "mongodb":{
      "pre":{
         "conn_id": "mongo-db",
         "database": "historicRiskParams",
         "collection": "test"
      },
      "prod":{
         "conn_id": "mongo-db",
         "database": "test",
         "collection": "historicRiskParams"
      }
   }
}

def get_environment():
    env = Variable.get('pre_prod')
    print("Environment is:", env)
    return env

def getLatestTime(env, **context):
    hook = MongoHook(conn_id=relation1['mongodb'][env]['conn_id'])
    collection = hook.get_collection(mongo_collection = relation1['mongodb'][env]['collection'],
                                  mongo_db = relation1['mongodb'][env]['database'])
    print("collection is: ", collection)
    cursor = collection.find({})
    print("cursor is: ", cursor)
    data = list(cursor)
    print("data is: ", data)
    df = pd.DataFrame(data)
    maxtime = df['lastmodifieddate'].max()
    context['ti'].xcom_push(key = 'lastesttime', value = maxtime)

def queryData(env, **context):
    maxtime = context['ti'].xcom_pull(key = "lastesttime", task_ids= ['get-latest-time'])[0]
    maxtime = str(maxtime)[:10]
    query = f"""SELECT id AS external_id, lastmodifieddate, data
    FROM fast-nexus-262509.salesforce.quote 
    WHERE lastmodifieddate > '{maxtime}'"""
    print(query)
    hook = BigQueryHook(bigquery_conn_id=relation1['bigquery'][env]['conn_id'],
                        use_legacy_sql=False)
    results_df = hook.get_pandas_df(sql=query)
    results_df.loc[:, 'lastmodifieddate'] = results_df['lastmodifieddate'].apply(lambda x: str(x))
    data = results_df.to_dict(orient = 'records')
    context['ti'].xcom_push(key = 'upload_data', value = data)

def uploadData(env, **context):
    data = context['ti'].xcom_pull(key = 'upload_data', task_ids = ['query-data'])
    print("original data:", data)
    data = data[0]
    print("data type is", type(data))
    print(data)
    hook = MongoHook(conn_id=relation1['mongodb'][env]['conn_id'])
    collection = hook.get_collection(mongo_collection=relation1['mongodb'][env]['collection'],
                                     mongo_db=relation1['mongodb'][env]['database'])
    collection.insert_many(data)


default_args = {
    'owner': 'airflow',
    'start_date' : datetime(2020, 1, 1),
    'retries' : 1,
    'retry_delay' : timedelta(minutes=2),
    'catchup': False
}

dag = DAG(
    dag_id = 'mongo-db',
    default_args = default_args,
    description = 'Data pipeline to upload quotes data in bigquery to MongoDB',
    schedule_interval = '@daily'
)

def main_pipeline(dag_):

    env = get_environment()

    t0 = PythonOperator(task_id = 'get-latest-time',
                        python_callable=getLatestTime,
                        op_kwargs={'env': env},
                        dag = dag_,
                        do_xcom_push=True)

    t1 = PythonOperator(task_id = 'query-data',
                        python_callable=queryData,
                        op_kwargs={'env': env},
                        dag=dag_,
                        do_xcom_push=True)

    t2 = PythonOperator(task_id = 'upload-data',
                        python_callable = uploadData,
                        provide_context=True,
                        op_kwargs = {'env': env},
                        dag = dag_)

    t0 >> t1 >> t2

main_pipeline(dag)