import pandas as pd
import urllib.parse
import json
from pymongo import MongoClient

def read_config_secret(path):
    f = open(path, "r")
    config_secret = f.read().replace('\n', '')
    f.close()
    return config_secret

def get_client(host, user, password, db_name="admin"):
    user = urllib.parse.quote_plus(user)
    password = urllib.parse.quote_plus(password)
    client = MongoClient(host, port = 27017,
                         username = user,
                         password = password,
                         authSource = db_name)
    return client

def mainClient(config_path, user_path, pwd_path):
    user = read_config_secret(user_path)
    password = read_config_secret(pwd_path)
    host = read_config_secret(config_path)
    client = get_client(host, user, password)
    return client

def getData(client, db_name, collection_name):
    db = client[db_name]
    collection = db[collection_name]
    cursor = collection.find({})
    data = list(cursor)
    df = pd.DataFrame.from_dict(data)
    df = df[['data']]
    df_final = pd.DataFrame(df['data'].values.tolist())
    return df_final

def processDtypes(df):
    df.loc[:, 'surface'] = df['surface'].apply(lambda x: int(x))
    df.loc[:, 'inaugurartion_year']  = df['inauguration_year'].apply(lambda x: int(x))
    df.loc[:, 'locationRisk'] = df['locationRisk'].apply(lambda x: int(x))
    df.loc[:, 'ownership'] = df['ownership'].apply(lambda x: int(x))
    df.loc[:, 'money_invested'] = df['money_invested'].apply(lambda x: float(x))
    df.loc[:, 'furnitureWithEquipment'] = df['furnitureWithEquipment'].apply(lambda x: float(x))
    df.loc[:, 'merchandise'] = df['merchandise'].apply(lambda x: float(x))
    df.loc[:, 'gross_income'] = df['gross_income'].apply(lambda x: float(x))
    return df

def main(config_path, user_path, pwd_path, db_name, collection_name):
    client = mainClient(config_path, user_path, pwd_path)
    df = getData(client, db_name, collection_name)
    df = processDtypes(df)
    return df