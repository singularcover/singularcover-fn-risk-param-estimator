from flask import jsonify, request
import read_historical
import price_estimator
import json

#CONFIG and SECRET
config_path = "/configs/default/percona-db-stg/MONGO_DB_HOST"
secret_path = "/secrets/default/percona-db-stg/MONGO_PASSWORD"
user_path = "/secrets/default/percona-db-stg/MONGO_USER"

#JSON LOAD
zipcode_dict = {
  "08": "0",
  "28": "1"
}

input_type = {
   "zipcode":"categorical",
   "surface":"continuous",
   "inauguration_year":"continuous",
   "locationRisk":"categorical",
   "ownership":"categorical",
   "money_invested":"continuous",
   "furnitureWithEquipment":"continuous",
   "merchandise":"continuous",
   "gross_income":"continuous"
}

#MONGO DB
db_name = "admin"
collection_name = "historicRiskParams"

def return_price():
    #NEED TO PUT FOR FILTERING MATRIZ EQUIVALENCIA
    input_data = request.get_json()
    df_historical = read_historical.main(config_path, user_path, secret_path, db_name, collection_name)
    values = price_estimator.main(df_historical, zipcode_dict, input_data, input_type)
    final_values = {}
    for key in list(input_type.keys()):
        final_values[key] = values[key]
    return jsonify(final_values)

##NEED TO ADD A POST REQUEST TO THE MONGODB DATABASE DIFFERENT COLLECTION WITH THE INPUT DATA
### AND THE OUTPUT DATA --> OR: WE NEED TO DO A POST REQUEST WITH THE QUOTE ID AND THE
### THE OUTPUT OF THE RISK PARAM ESTIMATOR?? THE ONLY PROBLEM IS THAT HOW WOULD WE GET THE
### THE ID OF THE QUOTE?? Need to think of a way to think this through..for now we can mock this as
### passing the quoteId information so that then we can cruzar the output with the historic IDs
### Or actually, even easier would be without just storing all the info
