import pandas as pd
from scipy import stats
import numpy as np
pd.options.mode.chained_assignment = None

def zip_label(x, zipcode_dict):
    if x[:2] not in zipcode_dict.keys():
        return str(int(max(zipcode_dict.values())) + 1)
    else:
        return zipcode_dict[x[:2]]

def dfFormat(df, zipcode_dict):
    df.loc[:, 'zipcode'] = df['zipcode'].apply(lambda x: zip_label(x, zipcode_dict))
    return df

def filter_input(dict_input, dict_type, zipcode_dict):
    input_fields_dict = {}
    missing_fields_dict = {}
    for key, value in dict_input.items():
        if value != '' and key == 'zipcode':
            input_fields_dict[key] = zip_label(value, zipcode_dict)
        elif value == '':
            missing_fields_dict[key] = dict_type[key]
        elif value != '':
            input_fields_dict[key] = value
    return input_fields_dict, missing_fields_dict

def process_categorical(df, variable, input_):
    df_slice = df.copy()
    df_slice = df_slice.loc[df_slice[variable] == input_]
    return df_slice

def process_continuous(df, variable, input_):

    def custom_percentile_score(x):
        if x <= 0.25:
            return 1
        elif x <= 0.75:
            return 2
        elif x <= 1:
            return 3

    df_slice = df.copy()
    df_slice.loc[:, 'rank'] = df_slice[variable].rank(pct=True)
    score = stats.percentileofscore([float(x) for x in df_slice[variable].to_list()], float(input_)) / 100
    df_slice.loc[:, 'score'] = df_slice['rank'].apply(lambda x: custom_percentile_score(x))
    df_slice = df_slice.loc[df_slice['score'] == custom_percentile_score(score)]
    return df_slice.drop(['rank', 'score'], axis=1)

def filter_data(df_original, dict_type, input_fields_dict):
    df = df_original.copy()
    df_list = []
    i = 0
    for key, value in input_fields_dict.items():
        if dict_type[list(dict_type.keys())[i]] == 'continuous':
            df_slice = process_continuous(df, key, value)
        elif dict_type[list(dict_type.keys())[i]] == 'categorical':
            df_slice = process_categorical(df, key, value)
        df_list.append(df_slice)
        i += 1
    return df_list

def main_process(df_list, input_fields_dict, missing_fields_dict):
    mean_dict = {key: '' for key in list(missing_fields_dict.keys())}
    for var, type_ in missing_fields_dict.items():
        if type_ == 'categorical':
            max_values = [df_[var].max() for df_ in df_list]
            max_values = [x for x in max_values if str(x).lower() != 'nan']
            mean_dict[var] = max(max_values)
        elif type_ == 'continuous':
            mean_values = []
            for df_ in df_list:
                values_list = [float(x) for x in df_[var]]
                mean_values.append(np.mean(values_list))
            mean_values = [x for x in mean_values if str(x).lower() != 'nan']
            mean_dict[var] = str(int(np.mean(mean_values)))
    input_filtered = {}
    for key, value in input_fields_dict.items():
        if value != '':
            input_filtered[key] = value
        elif value == '':
            break
    final_dict = {**input_filtered, **mean_dict}
    return final_dict

def main(df, zipcode_dict, dict_input, dict_type):
    input_fields_dict, missing_fields_dict = filter_input(dict_input, dict_type, zipcode_dict)
    df_list = filter_data(df, dict_type, input_fields_dict)
    final_dict = main_process(df_list, input_fields_dict, missing_fields_dict)
    final_dict['zipcode'] = dict_input['zipcode']
    for key, value in final_dict.items():
        final_dict[key] = str(value)
    return final_dict